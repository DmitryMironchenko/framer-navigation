import * as React from 'react'
import { Override, motionValue, useTransform } from "framer"
 
const contentOffsetY = motionValue(0)
const regularHeight = 56
const condensedHeight = 30
 
// Apply this override to your scroll component
export function TrackScroll(): Override {
    return { contentOffsetY: contentOffsetY }
}
 
// Apply this override to your parallax layer
export function AppBar(): Override {
    const height = useTransform(contentOffsetY, [0, condensedHeight-regularHeight, condensedHeight-regularHeight], [regularHeight, condensedHeight, condensedHeight], {
        clamp: false,
    })
    return {
        height: height,
    }
}

export const Logo: Override = () => {
    const opacity = useTransform(contentOffsetY, [0, condensedHeight-regularHeight, condensedHeight-regularHeight], [1, 0, 0], {
        clamp: false,
    })
    return{
        opacity: opacity
    }
}

export const scrolledLogo: Override = () => {
    const opacity = useTransform(contentOffsetY, [0, condensedHeight-regularHeight, condensedHeight-regularHeight], [0, 1, 1], {
        clamp: false,
    })
    return{
        opacity: opacity
    }
}