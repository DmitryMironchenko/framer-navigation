import * as React from "react"
import { Override, Data, useCycle } from "framer"

const data = Data({
    Button1isHover: false,
    Button1isClicked: false,
    Button2isHover: false,
    Button2isClicked: false,
    Button3isHover: false,
    Button3isClicked: false,
    Button4isHover: false,
    Button4isClicked: false,
})

//--------------Button1--------------

export const Button1: Override = () => {
    return {
        style: { cursor: "pointer" },
        onHoverStart() {
            data.Button1isHover = !data.Button1isHover
        },
        onHoverEnd() {
            data.Button1isHover = !data.Button1isHover
        },
        onTap() {
            data.Button1isClicked = !data.Button1isClicked
        },
        onTapStart() {
            data.Button1isClicked = !data.Button1isClicked
        },
    }
}

export const Button1isHover: Override = () => {
    return {
        opacity: data.Button1isHover ? 0 : 1,
    }
}

export const Button1isClicked: Override = () => {
    return {
        opacity: data.Button1isClicked ? 0 : 1,
    }
}

//--------------Button2--------------

export const Button2: Override = () => {
    return {
        style: { cursor: "pointer" },
        onHoverStart() {
            data.Button2isHover = !data.Button2isHover
        },
        onHoverEnd() {
            data.Button2isHover = !data.Button2isHover
        },
        onTap() {
            data.Button2isClicked = !data.Button2isClicked
        },
        onTapStart() {
            data.Button2isClicked = !data.Button2isClicked
        },
    }
}

export const Button2isHover: Override = () => {
    return {
        opacity: data.Button2isHover ? 0 : 1,
    }
}

export const Button2isClicked: Override = () => {
    return {
        opacity: data.Button2isClicked ? 0 : 1,
    }
}

//--------------Button3--------------

export const Button3: Override = () => {
    return {
        style: { cursor: "pointer" },
        onHoverStart() {
            data.Button3isHover = !data.Button3isHover
        },
        onHoverEnd() {
            data.Button3isHover = !data.Button3isHover
        },
        onTap() {
            data.Button3isClicked = !data.Button3isClicked
        },
        onTapStart() {
            data.Button3isClicked = !data.Button3isClicked
        },
    }
}

export const Button3isHover: Override = () => {
    return {
        opacity: data.Button3isHover ? 0 : 1,
    }
}

export const Button3isClicked: Override = () => {
    return {
        opacity: data.Button3isClicked ? 0 : 1,
    }
}

//--------------Button4--------------

export const Button4: Override = () => {
    return {
        style: { cursor: "pointer" },
        onHoverStart() {
            data.Button4isHover = !data.Button4isHover
        },
        onHoverEnd() {
            data.Button4isHover = !data.Button4isHover
        },
        onTap() {
            data.Button4isClicked = !data.Button4isClicked
        },
        onTapStart() {
            data.Button4isClicked = !data.Button4isClicked
        },
    }
}

export const Button4isHover: Override = () => {
    return {
        opacity: data.Button4isHover ? 0 : 1,
    }
}

export const Button4isClicked: Override = () => {
    return {
        opacity: data.Button4isClicked ? 0 : 1,
    }
}

export const PointerButton: Override = () => ({
    style: { cursor: "pointer" },
})
