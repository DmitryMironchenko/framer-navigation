// WARNING: this file is auto generated, any changes will be lost
import { createDesignComponent, CanvasStore } from "framer"
const canvas = CanvasStore.shared(); // CANVAS_DATA;
export const Active = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_JmouZcNYA", {}, 100,26);
export const Auction_NavButton = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_LjqlAhBiD", {}, 100,40);
export const Chevron_Vertical = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_zJumVP7Ia", {}, 24,24);
export const Dropdown_auctions = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_he5nb2OZi", {}, 1440,142);
export const Dropdown_buyAndSell = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_lpFjV3pwI", {}, 1440,142);
export const Dropdown_explore = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_PilOtpXPQ", {}, 1440,142);
export const Dropdown_private_sales = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_mVc47AEMA", {}, 1440,142);
export const Hover = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_ijy55gtQb", {}, 100,26);
export const Normal = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_GQP93rRGW", {}, 126,26);
export const Search_NavButton = createDesignComponent<{parentSize?:{width:number|string,height:number|string},width?:number|string,height?:number|string}>(canvas, "id_b3tJL7r75", {}, 100,40);
