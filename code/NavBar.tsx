import * as React from "react"
    import { Override, Data } from "framer"
    
    // Learn more: https://framer.com/docs/overrides/
    
    const state = Data({
        isHover: false,
        width:{
            active: 66,
            inactive: 0
        }
    })
    
    export function NavButton(props): Override {
        return {
            onHoverStart(){
                state.isHover=true
            },
            onHoverEnd(){
                state.isHover=false
            }
        }
    }

    export const Underline: Override = () => {
        return{
            animate: {width: state.isHover ? state.width.active : state.width.inactive}
        }
    }
