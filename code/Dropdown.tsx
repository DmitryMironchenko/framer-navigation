import * as React from "react"
import { Override, motionValue, useTransform, Data } from "framer"

const hover = Data({
    privateSales: false,
    auctions: false,
    buySell: false,
    explore: false,
    myAccount: false,
    search: false,
})

// ---------------------------- PRIVATE SALE Button ------------------------------

export function PrivateSales_Nav(): Override {
    return {
        style: {
            cursor: "pointer",
            backgroundColor: hover.privateSales
                ? "rgb(245, 245, 245)"
                : "rgba(255, 255, 255, 0)",
        },
        onHoverStart(event, info) {
            hover.privateSales = true
        },
        onHoverEnd(event, info) {
            hover.privateSales = false
        },
    }
}

export const PrivateSalesDropdown: Override = () => {
    return {
        onHoverStart(event, info) {
            hover.privateSales = true
        },
        onHoverEnd(event, info) {
            hover.privateSales = false
        },
        height: hover.privateSales ? 142 : 0,
        opacity: hover.privateSales ? 1 : 0,
    }
}

export const PrivateUnderline: Override = () => {
    return {
        animate: { width: hover.privateSales ? 97 : 0 },
    }
}

// ---------------------------- AUCTIONS Button ------------------------------

export function Auctions_Nav(): Override {
    return {
        style: {
            cursor: "pointer",
            backgroundColor: hover.auctions
                ? "rgb(245, 245, 245)"
                : "rgba(255, 255, 255, 0)",
        },
        onHoverStart() {
            hover.auctions = true
        },
        onHoverEnd() {
            hover.auctions = false
        },
    }
}

export const AuctionUnderline: Override = () => {
    return {
        animate: { width: hover.auctions ? 66 : 0 },
    }
}

export const AuctionsDropdown: Override = () => {
    return {
        onHoverStart() {
            hover.auctions = true
        },
        onHoverEnd() {
            hover.auctions = false
        },
        height: hover.auctions ? 142 : 0,
        opacity: hover.auctions ? 1 : 0,
    }
}

// ---------------------------- EXPLORE Button ------------------------------

export function Explore_Nav(): Override {
    return {
        style: {
            cursor: "pointer",
            backgroundColor: hover.explore
                ? "rgb(245, 245, 245)"
                : "rgba(255, 255, 255, 0)",
        },
        onHoverStart() {
            hover.explore = true
        },
        onHoverEnd() {
            hover.explore = false
        },
    }
}

export const ExploreDropdown: Override = () => {
    return {
        onHoverStart() {
            hover.explore = true
        },
        onHoverEnd() {
            hover.explore = false
        },
        height: hover.explore ? 142 : 0,
        opacity: hover.explore ? 1 : 0,
    }
}

// ---------------------------- BUY AND SELL Button ------------------------------

export function BuySell_Nav(): Override {
    return {
        style: {
            cursor: "pointer",
            backgroundColor: hover.buySell
                ? "rgb(245, 245, 245)"
                : "rgba(255, 255, 255, 0)",
        },
        onHoverStart() {
            hover.buySell = true
        },
        onHoverEnd() {
            hover.buySell = false
        },
    }
}

export const BuySellDropdown: Override = () => {
    return {
        onHoverStart() {
            hover.buySell = true
        },
        onHoverEnd() {
            hover.buySell = false
        },
        height: hover.buySell ? 142 : 0,
        opacity: hover.buySell ? 1 : 0,
    }
}

// ---------------------------- MY ACCOUNT Button ------------------------------

export function MyAccount_Nav(): Override {
    return {
        style: {
            cursor: "pointer",
            backgroundColor: hover.myAccount
                ? "rgb(245, 245, 245)"
                : "rgba(255, 255, 255, 0)",
        },
        onHoverStart() {
            hover.myAccount = true
        },
        onHoverEnd() {
            hover.myAccount = false
        },
    }
}

export const MyAccount_Dropdown: Override = () => {
    return {
        onHoverStart() {
            hover.myAccount = true
        },
        onHoverEnd() {
            hover.myAccount = false
        },
        height: hover.myAccount ? 142 : 0,
        opacity: hover.myAccount ? 1 : 0,
    }
}

// ---------------------------- MY ACCOUNT Button ------------------------------

export function Search_Nav(): Override {
    return {
        style: {
            cursor: "pointer",
            backgroundColor: hover.search
                ? "rgb(245, 245, 245)"
                : "rgba(255, 255, 255, 0)",
        },
        onHoverStart() {
            hover.search = true
        },
        onHoverEnd() {
            hover.search = false
        },
    }
}

export const Search_Dropdown: Override = () => {
    return {
        onHoverStart() {
            hover.search = true
        },
        onHoverEnd() {
            hover.search = false
        },
        height: hover.search ? 142 : 0,
        opacity: hover.search ? 1 : 0,
    }
}
